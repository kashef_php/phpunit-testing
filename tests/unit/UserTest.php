<?php

class UserTest extends \PHPUnit_Framework_TestCase
{

    public function testThatWeCanGetTheFirstName()
    {
        $user = new \App\Models\User;

        $user->setFirstName('majid');

        $this->assertSame($user->getFirstName(), 'majid');
    }

    public function testThatWeCanGetTheLastName()
    {
        $user = new \App\Models\User;

        $user->setLastName('kashef');

        $this->assertSame($user->getLastName(), 'kashef');
    }

    public function testFullNameIsReturned()
    {
        $user = new \App\Models\User;
        $user->setFirstName('majid');
        $user->setLastName('kashef');

        $this->assertSame($user->getFullName(), 'majid kashef');
    }

    public function testFirstAndLastNameAreTrimes()
    {
        $user = new \App\Models\User;
        $user->setFirstName('  majid    ');
        $user->setLastName('   kashef');

        $this->assertSame($user->getFirstName(), 'majid');
        $this->assertSame($user->getLastName(), 'kashef');
    }

    public function testEmailAddressCanBeSet()
    {
        $user = new App\Models\User;
        $user->setEmail('kashefymajid1992@gmail.com');

        $this->assertSame($user->getEmail(), 'kashefymajid1992@gmail.com');
    }

    public function testEmailVariablesContainCorrectValues()
    {
        $user = new \App\Models\User;
        $user->setFirstName('majid');
        $user->setLastName('kashef');
        $user->setEmail('kashefymajid1992@gmail.com');

        $emailVariables = $user->getEmailVariables();

        $this->assertArrayHasKey('full_name', $emailVariables);
        $this->assertArrayHasKey('email', $emailVariables);

        $this->assertSame($emailVariables['full_name'], 'majid kashef');
        $this->assertSame($emailVariables['email'], 'kashefymajid1992@gmail.com');
    }

}